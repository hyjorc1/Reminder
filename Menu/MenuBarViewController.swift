//
//  MenuBarViewController.swift
//  Reminder
//
//  Created by Jason on 2017/11/4.
//  Copyright © 2017年 Iowa State University Com S 309. All rights reserved.
//

import UIKit
import Firebase

class MenuBarViewController: UIViewController,UITableViewDelegate{

    
    var CurrentFeature:Int? = -1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.transitioningDelegate=AppDelegate.CustomTransition
        
        MenuBarViewController.checkUserPrioority()
    }

    @IBOutlet weak var TableContentView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        self.view.layer.shadowOpacity = 1
        self.view.layer.shadowRadius = 5
        TableContentView.clipsToBounds=true

        if let ThemeColor=ReminderStandardViewController.Theme((UserDefaults.standard.object(forKey: "BackGroundTheme") ?? "Default") as! String).SubColor
        {
            MenuBarView.backgroundColor=ThemeColor
        }
        

    }
    
    

    @IBAction func Dismiss(_ sender: Any) {
        

        dismiss(animated: true, completion: nil)

        
    }
    
    @IBOutlet weak var MenuBarView: UIView!
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    var NewFeatureTransit=NewFeatureTransitioning()

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        if(section == 0)
        {

            return 0.1
        }
        return 35
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if (indexPath.row == CurrentFeature!)
        {
            dismiss(animated: true, completion: nil)
            return
        }
        
        switch indexPath.row{
        case 0:
            performSegue(withIdentifier: "StandardTask", sender: self)
        case 1:
            performSegue(withIdentifier: "SharingList", sender: self)
        case 2:
            performSegue(withIdentifier: "ShoppingList", sender: self)
        case 3:
            performSegue(withIdentifier: "ExpenseTracker", sender: self)
        case 4:
            
            var LoginVC:UIViewController
            if(ShoppingListServerObject.getUid() == nil)
            {
                LoginVC=MenuAccountLoginViewController()
                LoginVC.transitioningDelegate=NewFeatureTransit
                present(LoginVC, animated: true, completion: nil)

            }else{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let ProfileVC=storyboard.instantiateViewController(withIdentifier: "MenuProfileEditor")

                ProfileVC.transitioningDelegate=NewFeatureTransit
                present(ProfileVC, animated: true, completion: nil)
            }
            



        case 5:

            MenuBarViewController.checkUserPrioority()

            if(MenuBarViewController.Status != "regular")
            {
                performSegue(withIdentifier: "Setting", sender: self)
                break;
            }
            
            
            alert(msg: "Please Upgrade Your Account")

            break;
            

        default:
            break
        }
    }
    static var Status:String="regular"
    static func checkUserPrioority(){

        if(ShoppingListServerObject.getUid() != nil)
        {
            let userRef = DatabaseService.shared.userRef.child((Auth.auth().currentUser?.uid)!)


            userRef.observe(DataEventType.value, with: {(snapshot) in
                let CurrentUser = FbUser(uid: (ShoppingListServerObject.getUid())!, dict: (snapshot.value as! [String : Any]))
                self.Status=CurrentUser?.status ?? "regular"

                if self.Status=="regular"
                {
                    UserDefaults.standard.set("Default", forKey: "BackGroundTheme")
                }

            })

        }else{
            UserDefaults.standard.set("Default", forKey: "BackGroundTheme")
        }
    }
    
    
    func alert(msg: String) {
        let alert = UIAlertController(title: msg, message: nil, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if(segue.identifier=="EmbedSegue")
        {
            let ToVC=segue.destination as! MenuTableViewController
            ToVC.tableView.delegate=self
            ToVC.ToSelect=CurrentFeature
  
            

        }else{
            segue.destination.transitioningDelegate=AppDelegate.NewFeatureTransition
        }
        

    }
 

}
